package ua.pp.voronin.serhii.tommy;

import ua.pp.voronin.serhii.tommy.exception.PackagingException;
import ua.pp.voronin.serhii.tommy.model.packaging.Box;
import ua.pp.voronin.serhii.tommy.model.part.Cube;
import ua.pp.voronin.serhii.tommy.model.part.Part;
import ua.pp.voronin.serhii.tommy.model.part.Sphere;
import ua.pp.voronin.serhii.tommy.model.part.Tetrahedron;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class PartsProcessor {

    public void packageParts(Collection<Part> parts, Collection<Box> boxes, int boxPackagingPricePerMeter) {
        // Наявні коробки
        List<Box> remainingBoxesSortedList = new ArrayList<>(boxes);

        // Сортуємо ящики від малих до великих, щоб спросити пошук. Будемо перебирати від малих до великих
        remainingBoxesSortedList.sort(Comparator.comparing(Box::getSide));

        for (Part part : parts) {
            try {
                Box box = selectSmallestBox(part, remainingBoxesSortedList);
                int boxPrice = box.calculateBoxPrice(boxPackagingPricePerMeter);
                reportRecommendedPackaging(part, box, boxPrice);
                remainingBoxesSortedList.remove(box); // Підказка: як ми можемо краще назвати змінну boxesList, щоб була зрозуміла мета цієї операції?
            } catch (PackagingException e) {
                reportUnableToPackage(part);
            }
        }
    }

    private Box selectSmallestBox(Part part, List<Box> availableSortedBoxes) {
        // Залежно від типу деталі розраховуємо для для неї потрібний розмір коробки
        int boxSizeRequired = calculateBoxSizeRequired(part);

        // Шукаємо об'єм деталі для визначення ваги
        int volume = calculatePartVolume(part);

        int weight = (int) Math.ceil(volume * part.getDensity());
        double protectiveLayerThickness = part.getProtectiveLayerThickness(weight);

        int partSizeWithProtectiveLayer = (int) Math.ceil(boxSizeRequired + protectiveLayerThickness * 2);

        for (Box box : availableSortedBoxes) {
            if (box.getSide() >= partSizeWithProtectiveLayer) {
                return box;
            }
        }

        // Якщо в циклі ми не знайшли жодного підходящого ящика, повідомимо про це за допомогою кидання виключення
        throw new PackagingException();
    }

    private static int calculatePartVolume(Part part) {
        int volume = 0;
        if (part instanceof Cube) {
            int side = ((Cube) part).getSide();
            volume = side * side * side;
        } else if (part instanceof Sphere) {
            int radius = ((Sphere) part).getRadius();
            double sphereVolume = 4d / 3 * Math.PI * radius * radius * radius; // Об'єм кулі рахуємо як 4/3πr³
            volume = (int) Math.ceil(sphereVolume); // Заокруглюємо отримане значення вгору
        } else if (part instanceof Tetrahedron) {
            int side = ((Tetrahedron) part).getSide();
            double tetrahedronVolume = Math.sqrt(2) / 12 * side * side * side; // Об'єм правильного тетраедру: (√2/12)a³
            volume = (int) Math.ceil(tetrahedronVolume); // Заокруглюємо отримане значення вгору.
        }
        return volume;
    }

    private int calculateBoxSizeRequired(Part part) {
        int boxSizeRequired = switch (part) {
            case Cube c -> c.getSide();
            case Sphere s -> s.getRadius() * 2;
            case Tetrahedron t -> (int) Math.ceil(t.getSide() / Math.sqrt(2));
        };
        return boxSizeRequired;
    }

    private void reportRecommendedPackaging(Part part, Box box, int price) {
        String message = String.format(
                "Деталь %s варто покласти у ящик %s. Вартість пакування: %.2f₴",
                part, box, price / 100d);
        System.out.println(message);
    }

    private void reportUnableToPackage(Part part) {
        String message = String.format(
                "Деталь %s не вдалося розмістити у наявні ящики",
                part);
        System.out.println(message);
    }
}
